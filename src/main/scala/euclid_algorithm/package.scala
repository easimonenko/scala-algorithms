package euclid_algorithm

package object recursive {
  def gcd(a: Int, b: Int): Int =
    if (b == 0)
      a
    else
      gcd(b, a % b)
}

package object iterative {
  def gcd(a: Int, b: Int) = {
    var a2 = a
    var b2 = b
    while (b2 != 0) {
      a2 %= b2
      val t = a2
      a2 = b2
      b2 = t
    }
    a
  }
}
