package fibonacci_numbers

package object recursive {
  def fib(n: Int): Int = n match {
    case 0 => 0
    case 1 => 1
    case _ => fib(n - 1) + fib(n - 2)
  }
}

package object iterative {
  def fib(n: Int) = {
    var first = 0
    var second = 1
    var m = 0
    while (m < n) {
      val next = first + second
      first = second
      second = next
      m += 1
    }
    first
  }
}

package object pseudoiterative {
  def fib(n: Int) = {
    def iter(n: Int, k: Int, first: Int, second: Int): Int = {
      if (k == n)
        first
      else
        iter(n, k + 1, second, first + second)
    }
    iter(n, 0, 0, 1)
  }
}
