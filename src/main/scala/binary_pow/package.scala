package binary_pow

package object recursive {
  def binpow(a: Int, n: Int): Int =
    if (n == 0)
      1
    else if (n % 2 == 1)
      binpow(a, n - 1) * a
    else
      binpow(a * a, n / 2)
}

package object iterative {
  def binpow(a: Int, n: Int): Int = {
    var result = 1
    var m = n
    var b = a
    while (m > 0)
      if ((m & 1) == 1) {
        result *= b
        m -= 1
      }
      else {
        b *= b
        m >>= 1
      }
    result
  }
}

package object bigint {
  def binpow(a: BigInt, n: Int): BigInt = {
    a.pow(n)
  }
}