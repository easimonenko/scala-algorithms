import euclid_algorithm._

recursive.gcd(1, 0)
recursive.gcd(1, 1)
recursive.gcd(1, 2)
recursive.gcd(2, 3)
recursive.gcd(2, 4)
recursive.gcd(15, 25)

iterative.gcd(1, 0)
iterative.gcd(1, 1)
iterative.gcd(1, 2)
iterative.gcd(2, 3)
iterative.gcd(2, 4)
iterative.gcd(15, 25)