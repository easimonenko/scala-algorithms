import binary_pow._

recursive.binpow(2, 16)
recursive.binpow(2, 15)

iterative.binpow(2, 16)
iterative.binpow(2, 15)

bigint.binpow(2, 16)
bigint.binpow(2, 15)
bigint.binpow(2, 100)
